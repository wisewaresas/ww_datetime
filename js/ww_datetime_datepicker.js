/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
	Drupal.behaviors.wwDatetimeDatepicker = {
		attach: function(context, settings) {
			$.each(Drupal.settings.wwDateTime, function(key, value) {
				$('#' + value.id).once('ww-datetime-procesed', function() {
					if (value.type == 'ww_date') {
						$(this).datepicker(value.settings);
					}
					else if (value.type == 'ww_datetime') {
						$(this).datetimepicker(value.settings);
					}
				});
			});
		}
	}
})(jQuery);

